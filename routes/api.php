<?php

use App\Http\Controllers\GuestlogController;
use App\Http\Controllers\Frontdesk\GuestlogController as FrontdeskGuestlogController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/guestlogs', [GuestlogController::class, 'store']);
Route::get('/guestlogs/{guestlog:uuid}', [GuestlogController::class, 'statusCheck']);
Route::get('/frontdesk/guestlogs', [FrontdeskGuestlogController::class, 'index']);
Route::put('/frontdesk/guestlogs/{guestlog:uuid}/status', [FrontdeskGuestlogController::class, 'updateStatus']);

// Route::middleware(['auth:sanctum'])->group(function () {
//     Route::get('/frontdesk/guestlogs', [FrontdeskGuestlogController::class, 'index']);
//     Route::put('/frontdesk/guestlogs/{guestlog}/status', [FrontdeskGuestlogController::class, 'updateStatus']);
// });
