<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGuestlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'entry_date' => ['required'],
            'entry_time' => ['required'],
            'name' => ['required'],
            'id_number' => ['required'],
            'email' => ['required'],
            'phone' => ['required'],
            'purpose' => ['required'],
            'photo' => ['required', 'image', 'max:8192']
        ];
    }
}
