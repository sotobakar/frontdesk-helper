<?php

namespace App\Http\Requests\Frontdesk;

use Illuminate\Foundation\Http\FormRequest;

class ListGuestlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'status' => ['in:pending,approved,finished,rejected'],
            'date' => ['date']
        ];
    }
}
