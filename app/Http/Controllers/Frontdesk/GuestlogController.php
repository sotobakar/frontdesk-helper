<?php

namespace App\Http\Controllers\Frontdesk;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontdesk\ListGuestlogRequest;
use App\Http\Requests\Frontdesk\UpdateGuestlogStatusRequest;
use App\Models\Guestlog;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class GuestlogController extends Controller
{
    public function index(ListGuestlogRequest $request)
    {
        $query = Guestlog::query();

        if ($request->filled('status')) {
            $query->where('status', '=', $request->query('status'));
        }

        if ($request->filled('date')) {
            $query->where('entry_date', '=', $request->query('date'));
        }

        // Execute query
        $guestlogs = $query->get();

        return response()->json([
            'data' => $guestlogs->toArray()
        ]);
    }

    public function updateStatus(UpdateGuestlogStatusRequest $request, Guestlog $guestlog)
    {
        // Validate request
        // Get request body
        $data = $request->all();

        // Insert status update time
        if ($request->input('status') == 'approved') {
            $data['approved_time'] = Carbon::now('GMT+7')->format('H:i:s');
        } elseif ($request->input('status') == 'finished') {
            $data['finished_time'] = Carbon::now('GMT+7')->format('H:i:s');
        }

        // Try update status
        $updateGuestlogStatus = $guestlog->update($data);
        if ($updateGuestlogStatus) {
            // TODO: Send to guest's email.
            return response()->json(null, 200);
        } else {
            return response()->json(null, 400);
        }
    }
}
