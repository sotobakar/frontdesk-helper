<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGuestlogRequest;
use App\Models\Guestlog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class GuestlogController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGuestlogRequest $request)
    {
        $uuid = Str::uuid();
        $photo = $request->file('photo');

        // Store to S3
        $storedFilename = Storage::putFileAs("guestlogs", $photo, "{$uuid}.{$photo->extension()}");

        $guestlog = new Guestlog(array_merge($request->validated(), [
            'uuid' => $uuid,
            'photo' => $storedFilename
        ]));

        if ($guestlog->save()) {
            // Send email with ticket id to customer
            return response()->json([
                'data' => $guestlog
            ], 201);
        } else {
            return response()->json([
                'message' => 'Guestlog form submission failed.'
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Guestlog  $guestlog
     * @return \Illuminate\Http\Response
     */
    public function statusCheck(Guestlog $guestlog)
    {
        return response()->json([
            'data' => $guestlog
        ], 200);
    }
}
