<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guestlogs', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->date("entry_date");
            $table->time("entry_time");
            $table->string("name");
            $table->string("id_number");
            $table->string("email");
            $table->string("purpose");
            $table->string("phone");
            $table->string("photo")->nullable();
            $table->enum("status", ["pending", "approved", "finished", "rejected"])->default("pending");
            $table->string("rejected_reason")->nullable();
            $table->time("approved_time")->nullable();
            $table->string("approved_message")->nullable();
            $table->time("finished_time")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guestlogs');
    }
};
